<?php

namespace Modules\Account\Http\Controllers;
use Modules\Order\Entities\Order;
class AccountDashboardController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $order = Order::where('status','pending_payment')->delete();

        return view('public.account.dashboard.index', [
            'account' => auth()->user(),
            'recentOrders' => auth()->user()->recentOrders(5),
        ]);
    }
}
