<nav class="navbar navbar-static-top clearfix">
    <ul class="nav navbar-nav clearfix">
        <li class="visit-store hidden-sm hidden-xs">
            <a href="{{ route('home') }}">
                <i class="fa fa-desktop"></i>
                {{ trans('admin::admin.visit_store') }}
            </a>
        </li>

        <li class="dropdown top-nav-menu pull-right">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user-circle-o"></i><span>{{ $currentUser->first_name }}</span>
            </a>

            <ul class="dropdown-menu">
                <li><a href="{{ route('admin.profile.edit') }}">{{ trans('user::users.profile') }}</a></li>
                <li><a href="{{ route('admin.logout') }}">{{ trans('user::auth.logout') }}</a></li>
            </ul>
        </li>

{{-- 
        <li class="dropdown dropdown-notification nav-item" style="float: right">
        @if(Auth::user()->unreadNotifications)
        {{dd(Auth::user()->unreadNotifications)}}
          <a class="nav-link nav-link-label readnotifocation" href="javascript:void(0)" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="badge notificationBadge">{{Auth::user()->unreadNotifications->count()}}</span></a>
      @else
          <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="badge notificationBadge">{{'0'}}</span></a>
      @endif
        

        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right" id="notifications" style="width: 300px;">
          <li class="dropdown-menu-header">
            <h6 class="dropdown-header m-0">
              <span class="grey darken-2">Notifications</span>
                @if(Auth::user()->unreadNotifications)
                  <span class="notification-tag badge badge-default badge-danger float-right m-0"> 
                  {{Auth::user()->unreadNotifications->count()}} New</span>
                @endif 
            </h6>
          </li>
          <li class="scrollable-container media-list w-100">
            @if(Auth::user()->Notifications)
              @foreach (Auth::user()->Notifications as $n)
                <a href="{{$n->data['actionURL']}}">
                  <div class="media">
                    <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                    <div class="media-body">
                      <h6 class="media-heading"><i class="fa fa-envelope"></i> {{$n->data['title']}}</h6> --}}
                      {{-- <p class="notification-text font-small-3 text-muted">{{$n->data['message']}}</p><small> --}}
                 {{--        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">{{$n->created_at->diffForHumans()}}</time></small>
                    </div>
                  </div>
                </a>
              @endforeach
            @endif
            </li> --}}
          {{-- <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="{{url('admin/all-notification')}}">Read all notifications</a></li> --}}
       {{--  </ul>
      </li> --}}

        @if (count(supported_locales()) > 1)
            <li class="language dropdown top-nav-menu pull-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span>{{ strtoupper(locale()) }}</span>
                </a>

                <ul class="dropdown-menu">
                    @foreach (supported_locales() as $locale => $language)
                        <li class="{{ $locale === locale() ? 'active' : '' }}">
                            <a href="{{ localized_url($locale) }}">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endif
    </ul>
</nav>


@push('styles')

<style>
  
.notificationBadge{
      position: absolute;
    top: 4px;
    right: 0px;
    width: 20px;
    height: 20px;
    padding: 4px;
    color: white;
    background: #e57569;
}
</style>

@endpush
@push('scripts')

  <script>
      
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


  //   $('.readnotifocation').click(function(){


  //     $.ajax({ 
  //           url: '/admin/readnotification',
  //           type: 'post',
  //           success: function(result)
  //           {
                
  //           }
  //       });
  // });


// setInterval(function(){ 

//         $.ajax({ 
//             url: '/admin/getnotification',
//             type: 'post',
//             success: function(result)
//             {

//                 if(result.length > 0){

//                   $('.notificationBadge').html(result.length);

//                 $.each(result, function(i, item) {

//                   var test = '<a href="'+item.actionURL+'"><div class="media"><div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div><div class="media-body"><h6 class="media-heading"><i class="fa fa-envelope"></i> '+item.title+'</h6><time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00"> '+item.updated_at+'</time></small></div>                  </div>                </a>';

//                   $(test).insertBefore('.media-list > a');


//                 })

//                 }
//             }
//         });



// }, 100000);



  </script>

@endpush