<?php

namespace Modules\Checkout\Http\Controllers;

use Exception;
use Notification;
use Modules\User\Entities\User;
use Modules\Order\Entities\Order;
use Modules\Payment\Facades\Gateway;
use Modules\Checkout\Events\OrderPlaced;
use FleetCart\Notifications\OrderNotification;

use Modules\Checkout\Services\OrderService;

class CheckoutCompleteController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param int $orderId
     * @param \Modules\Checkout\Services\OrderService $orderService
     * @return \Illuminate\Http\Response
     */
    public function store($orderId, OrderService $orderService)
    {

        $order = Order::findOrFail($orderId);

        $gateway = Gateway::get(request('paymentMethod'));

        try {
            $response = $gateway->complete($order);
        } catch (Exception $e) {
            $orderService->delete($order);

            return response()->json([
                'message' => $e->getMessage(),
            ], 403);
        }

        $order->storeTransaction($response);
        // $user = User::find(1);
        //             $details = [
        //                 'n_title'     => 'New Order ',
        //                 'title'       => 'you have a new order',
        //                 'message'     => 'you have a new order',
        //                 'actionURL'   => url('admin/orders/'.$order->id.'')
        //             ];

        //     Notification::send($user, new OrderNotification($details)); 
        event(new OrderPlaced($order));

        session()->put('placed_order', $order);

        if (! request()->ajax()) {
            return redirect()->route('checkout.complete.show');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $order = session('placed_order');

        if (is_null($order)) {
            return redirect()->route('home');
        }

        return view('public.checkout.complete.show', compact('order'));
    }
}
