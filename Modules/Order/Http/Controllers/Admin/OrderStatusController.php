<?php

namespace Modules\Order\Http\Controllers\Admin;

use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderProduct;
use Modules\Order\Events\OrderStatusChanged;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Modules\Order\Mail\OrderTrackingMail;


class OrderStatusController
{
    /**
     * Update the specified resource in storage.
     *
     * @param \Modules\Order\Entities\Order $request
     * @return \Illuminate\Http\Response
     */
    public function update(Order $order)
    {

        $message = '';
        if(request('status')=='processing' && !$order->tracking_id){
            try {

               $products = [];

            foreach ($order->products as $key => $value) {
                $products[$key]['name'] = $value->name;
                $products[$key]['price'] = $value->unit_price->format();
                $products[$key]['qty'] = $value->qty;
            }
             $post = array();
              $this->http_build_query_for_curl($products, $post);
            	$payment_method = '';
            // dd($products);
              if($order->payment_method=='Cash On Delivery'){ $payment_method = 'COD'; }else{$payment_method = 'prepaid'; }
            



                 $res = Http::withHeaders([
                          'NP-API-KEY' => '507c81c6157979b9adda174bd30eb1a3f77697fa34777',
                    ])->asForm()->post('https://ship.nimbuspost.com/api/orders/create', [
                        'order_number' => $order->id,
                            'payment_method' => $payment_method,
                        'amount' => $order->total->amount(),
                        'fname' => $order->shipping_first_name,
                        'lname' => $order->shipping_last_name,
                        'address' => $order->shipping_address_1,
                        'address_2' => $order->shipping_address_2,
                        'phone' => $order->customer_phone,
                        'city' => $order->shipping_city,
                        'state' => $order->shipping_state,
                        'country' => $order->shipping_country,
                        'pincode' => $order->shipping_zip,
                        'products' => $products,

                    ]);


                $result = $res->object();
                // dd($result);
                if($result->status == true){

                    $tracking_id = $result->data;

                     $order->update(['status' => request('status'),'tracking_id'=>$tracking_id  ]);

                      $this->adjustStock($order);
                    event(new OrderStatusChanged($order));
                     return   $message = trans('order::messages.status_updated');
                } 

                 return $message = $result->message;
                
            } catch (Exception $e) {
                $message =  $e->getMessage();
            }
        } 



        $order->update(['status' => request('status')]);

        $this->adjustStock($order);

        $message = trans('order::messages.status_updated');

        event(new OrderStatusChanged($order));
        


        return $message;
    }

    private function adjustStock(Order $order)
    {
        if (in_array(request('status'), [Order::CANCELED, Order::REFUNDED])) {
            $this->restoreStock($order);
        } else {
            $this->reduceStock($order);
        }
    }

    private function restoreStock(Order $order)
    {
        $order->products->each(function (OrderProduct $orderProduct) {
            $orderProduct->product->increment('qty', $orderProduct->qty);
            if($orderProduct->product->qty > 0){
                
                $orderProduct->product->increment('in_stock', 1);

            }else{

                $orderProduct->product->decrement('in_stock', 1);
            }
        });
    }

    private function reduceStock(Order $order)
    {
        $order->products->each(function (OrderProduct $orderProduct) {
            $orderProduct->product->decrement('qty', $orderProduct->qty);
            if($orderProduct->product->qty > 0){
                
                $orderProduct->product->increment('in_stock', 1);

            }else{

                $orderProduct->product->decrement('in_stock', 1);
            }
        });
    }


    
    public function updateTracking(Order $order){

       $order = Order::find(request('pk'));

  
        $order->update(['tracking_id' => request('value')]);
          Mail::to($order->customer_email)
            ->send(new OrderTrackingMail($order));

        $message = "Tracking Id Updated Successfully";

        return $message;

    }


 private function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {

        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }

        foreach ( $arrays AS $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : 'products['.$key.']';
            if ( is_array( $value ) OR is_object( $value )  ) {
                $this->http_build_query_for_curl( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
    }

}
