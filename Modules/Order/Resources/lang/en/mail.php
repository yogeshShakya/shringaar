<?php

return [
    'your_order_status_changed_subject' => 'Your order status is changed',
    'your_order_status_changed_text' => 'Your order #:order_id status is changed to :status.',
    'your_order_tracking_subject' => 'Track your address',
    'your_order_tracking_text' => 'Your order tracking ID #:tracking_id and click here for tracking :link',
];
