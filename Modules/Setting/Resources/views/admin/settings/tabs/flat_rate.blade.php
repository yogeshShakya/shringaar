<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('flat_rate_enabled', trans('setting::attributes.flat_rate_enabled'), trans('setting::settings.form.enable_flat_rate'), $errors, $settings) }}
        {{ Form::text('translatable[flat_rate_label]', trans('setting::attributes.translatable.flat_rate_label'), $errors, $settings, ['required' => true]) }}
        {{-- {{ Form::number('flat_rate_cost','Default '.trans('setting::attributes.flat_rate_cost'), $errors, $settings, ['min' => 0, 'required' => true]) }} --}}

        <input name="flat_rate_cost" class="form-control " id="flat_rate_cost" value="0.00" min="0" type="hidden">

    </div>
</div>


@php  
    $i = 0;
    $k = 1;
    $a = [];
    foreach($settings['flat_rate_cost2'] as $key => $f){

        $a[$key] = array_keys($f)[0];
    }


    function getflatV($v){

         foreach(setting('flat_rate_cost2') as $key => $f){
            $a[$key] = array_keys($f)[0];

            if(array_keys($f)[0] == $v){
                    return $f[array_keys($f)[0]];
            }
        }


    }


@endphp
    
{{-- @foreach($settings['flat_rate_cost2'] as $key=> $s) --}}
@foreach($settings['supported_countries'] as $key=> $s)
           
    @if(in_array($s, $a))
    <div class="row">        
        {{-- <div class="col-md-8">
        {{ Form::text('flat_rate_cost2[]', $s, $errors, $settings['flat_rate_cost2'][$i] ,['min' => 0, 'required' => true]) }}
        </div> --}}

        <div class="col-md-8">
            <div class="form-group">
                <label for="flat_rate_cost2[]" class="col-md-3 control-label text-left">{{ $s }}<span class="m-l-5 text-red">*</span></label>
                <div class="col-md-9">
                    <input name="flat_rate_cost2[{{ $i }}][{{ $s }}]" class="form-control " value="{{ getflatV($s) }}" min="0" type="number">
                 
                </div>
            </div>
        </div>
    </div>
  
    @else
        <div class="row">   

            <div class="col-md-8">
                <div class="form-group">
                    <label for="flat_rate_cost2[]" class="col-md-3 control-label text-left">{{ $s }}<span class="m-l-5 text-red">*</span></label>
                    <div class="col-md-9">
                        <input name="flat_rate_cost2[{{ $i }}][{{ $s }}]" class="form-control " value="0" min="0" type="number">
                     
                    </div>
                </div>
            </div>
        </div>
    @endif
      @php
        $i++;
        $k++;
    @endphp
@endforeach

