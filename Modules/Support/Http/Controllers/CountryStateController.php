<?php

namespace Modules\Support\Http\Controllers;

use Modules\Support\State;
use Modules\Support\Money;

class CountryStateController
{
    /**
     * Display a listing of the resource.
     *
     * @param string $countryCode
     * @return \Illuminate\Http\Response
     */
    public function index($countryCode)
    {
        $states = State::get($countryCode);

        // if(auth()->guest()){


        //     foreach(setting()->flat_rate_cost2 as $key=> $s){
        //              if(array_key_first($s) == $countryCode){

        //                 $p = $s[array_key_first($s)];
        //                 $d =  Money::inDefaultCurrency($p);
        //              }
        //        }
        //     // array_merge($states,$d);
        //     // dd(response()->json($states,$d));
        // }
        return response()->json($states);
    }
}
