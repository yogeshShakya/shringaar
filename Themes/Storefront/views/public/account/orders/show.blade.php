@extends('public.layout')

@section('title', trans('storefront::account.view_order.view_order'))

@section('breadcrumb')
    <li><a href="{{ route('account.dashboard.index') }}">{{ trans('storefront::account.pages.my_account') }}</a></li>
    <li><a href="{{ route('account.orders.index') }}">{{ trans('storefront::account.pages.my_orders') }}</a></li>
    <li class="active">{{ trans('storefront::account.orders.view_order') }}</li>
@endsection

@section('content')
    <section class="order-details-wrap">
        <div class="container">
            <div class="order-details-top">
                <h3 class="section-title">{{ trans('storefront::account.view_order.view_order') }}

                     @if($order->status == 'pending' || $order->status == 'on_hold' || $order->status == 'pending_payment')
                     <span class="float-right"><a href="javascript:void(0)" data-toggle="modal" onclick="$('#cancelOrderForm').trigger('reset');" data-target="#cancelPopup"  class="btn btn-sm btn-primary">Cancel Order</a></span>

                     @endif

                    @if($order->status == 'completed')
                     <span class="float-right"><a href="javascript:void(0)" data-toggle="modal" onclick="$('#returnOrderForm').trigger('reset');" data-target="#returnPopup"  class="btn btn-sm btn-primary">Return Order</a></span>

                     @endif

                </h3>

                <div class="row">
                    @include('public.account.orders.show.order_information')
                    @include('public.account.orders.show.billing_address')
                    @include('public.account.orders.show.shipping_address')
                </div>
            </div>

            @include('public.account.orders.show.items_ordered')
            @include('public.account.orders.show.order_totals')
        </div>
    </section>



     @if($order->status == 'pending' || $order->status == 'on_hold' || $order->status == 'pending_payment')
    <!-- Modal -->
    <div class="modal fade" id="cancelPopup" tabindex="-1" role="dialog" aria-labelledby="cancelPopupLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="cancelPopupLabel">Cancel Order</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <form action="{{ url('account/cancelOrder/'.$order->id.'')}}" id="cancelOrderForm">
                  <div class="modal-body">
                        @csrf
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <label for="">Reason For Order Cancellation</label>
                        <textarea name="cancel_reason" class="form-control" id=""></textarea>
                  </div>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
            </form>
        </div>
      </div>
    </div>
    @endif

     @if($order->status == 'completed')
    <!-- Modal -->
    <div class="modal fade" id="returnPopup" tabindex="-1" role="dialog" aria-labelledby="returnPopupLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="returnPopupLabel">Return Order</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <form action="{{ url('account/returnOrder/'.$order->id.'')}}" id="returnOrderForm">
                  <div class="modal-body">
                        @csrf
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <label for="">Reason For Return</label>
                        <textarea name="return_reason" class="form-control" id=""></textarea>
                  </div>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
            </form>
        </div>
      </div>
    </div>
    @endif
@endsection

