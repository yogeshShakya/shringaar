<div class="payment-method" v-cloak>
    <h4 class="title @guest accordion_head @endguest">{{ trans('storefront::checkout.payment_method') }} @guest <span class="plusminus">+</span> @endguest</h4>

    @guest
        <div class="acc-none">
    @endguest
    <div>
        
    
    <div class="payment-method-form">
        <div class="form-group">
            <div class="form-radio" v-for="(gateway, name) in gateways">
                <input
                    type="radio"
                    name="form.payment_method"
                    v-model="form.payment_method"
                    :value="name"
                    :id="name"
                >

                <label :for="name" v-text="gateway.label"></label>
                <span class="helper-text" v-text="gateway.description"></span>
            </div>

            <span class="error-message" v-if="hasNoPaymentMethod">
                {{ trans('storefront::checkout.no_payment_method') }}
            </span>
        </div>
        @guest
    </div>
    @endguest
</div>
    <div class="coupon-wrap">
    <div class="form-group">
        <div class="form-input">
            <input
                type="text"
                v-model="couponCode"
                placeholder="{{ trans('storefront::cart.enter_coupon_code') }}"
                class="form-control"
                @input="couponError = null"
            >

            <span
                class="error-message"
                v-if="couponError"
                v-text="couponError"
            >
            </span>
        </div>

        <button
            type="button"
            class="btn btn-primary btn-apply-coupon"
            :class="{ 'btn-loading': applyingCoupon }"
            @click.prevent="applyCoupon"
        >
            {{ trans('storefront::cart.apply_coupon') }}
        </button>
    </div>

    <span
        class="error-message"
        v-if="couponError"
        v-text="couponError"
    >
    </span>
</div>
    </div>
    

    
        </div>


<div id="stripe-card-element" v-show="form.payment_method === 'stripe'" v-cloak>
    {{-- A Stripe Element will be mounted here dynamically. --}}
</div>

<span class="error-message" v-if="stripeError" v-text="stripeError"></span>

<div class="payment-instructions" v-if="shouldShowPaymentInstructions" v-cloak>
    <h4 class="title">{{ trans('storefront::checkout.payment_instructions') }}</h4>

    <p v-html="paymentInstructions"></p>
</div>



