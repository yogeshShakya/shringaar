@extends('public.layout')

@section('title', setting('store_tagline'))

@section('content')
    @includeUnless(is_null($slider), 'public.home.sections.slider')

    <section class="cat-container">
        <div class="cat-text">
            shringaar
        </div>
        <div class="cat-log">
            <img src="{{ asset('themes/storefront/public/images/shring.png') }}" class="img-fluid">
        </div>
        <div class="cat-log2">
            <img class="img-fluid" src="{{ asset('themes/storefront/public/images/shring.png') }}">
        </div>
        <div class="container">
            <div class="ring-cat">
                <div class="row">
                    <div class="col-12 col-md-12 text-center">
                        <div class="sec-tit">
                            <h3>Categories</h3>
                        </div>
                    </div>
                </div>
                {{-- @if (setting('storefront_flash_sale_and_vertical_products_section_enabled'))
                    <flash-sale-and-vertical-products :data="{{ json_encode($flashSaleAndVerticalProducts) }}"></flash-sale-and-vertical-products>
                @endif --}}

            @if (setting('storefront_top_brands_section_enabled') && $topBrands->isNotEmpty())
                    @php $i=0; @endphp
                @foreach($topBrands as $brand)

                    @if($i%2 == 0)
                        <div class="row justify-content-center th align-items-center ">
                    
                            <div class="col-xl-6 col-lg-6 col-sm-6">
                                <div class="cat-box">
                                    <h2>{{ $brand['name']}}</h2>
                                    <div>
                                       {!! $brand['description'] !!}
                                    </div>
                                    <a class="btn btn-primary btn-submit" style="margin-top: 15px;" href="{{ $brand['url'] }}">Shop Now</a>
                                
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-6 col-sm-6">
                                <img src="{{ $brand['logo']->path }}" alt="">
                            </div>

                        </div>

                    @else
                        <div class="row justify-content-center th align-items-center ">
                            <div class="col-xl-4 col-lg-6 col-sm-6">
                                <img src="{{ $brand['logo']->path }}" alt="">
                            </div>
                            <div class="col-xl-6 col-lg-6 col-sm-6">
                                <div class="cat-box">
                                    <h2>{{ $brand['name']}}</h2>
                                    <div>
                                       {!! $brand['description'] !!}
                                    </div>
                                    <a class="btn btn-primary btn-submit" style="margin-top: 15px;" href="{{ $brand['url'] }}">Shop Now</a>
                                
                                </div>
                            </div>

                        </div>
                    @endif
                 
                @php $i++; @endphp
                @endforeach

         @endif

               {{--  <div class="row justify-content-center th align-items-center">
                    <div class="col-xl-4 col-lg-6">
                        @if (setting('storefront_top_brands_section_enabled') && $topBrands->isNotEmpty())
                            <top-brands :top-brands="{{ json_encode($topBrands) }}"></top-brands>
                        @endif
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="cat-box">
                            <h2>Mirayah</h2>
                            <div>
                                <p>Discover our finely curated collection of investment-worthy diamonds and gemstones, breathtaking designs, and exceptional craftsmanship</p>
                            </div>
                            <a class="border-button" href="#">Shop Now</a>
                        
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center th align-items-center">
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="cat-box">
                            <h2>Mirayah</h2>
                            <div>
                                <p>Discover our finely curated collection of investment-worthy diamonds and gemstones, breathtaking designs, and exceptional craftsmanship</p>
                            </div>
                            <a class="border-button" href="#">Shop Now</a>
                        
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-6">
                        @if (setting('storefront_top_brands_section_enabled') && $topBrands->isNotEmpty())
                            <top-brands :top-brands="{{ json_encode($topBrands) }}"></top-brands>
                        @endif
                    </div>

                </div>
                 --}}
                
            </div>
        </div>
    </section>

    <section class="product-bestsale-jewelry">
        <div class="container">
            <div class="row">
                    <div class="col-12 col-md-12 text-center">
                        <div class="sec-tit">
                            <h3>Best Selling Products</h3>
                        </div>
                    </div>
            </div>

            @if (setting('storefront_product_tabs_1_section_enabled'))
                <product-tabs-one :data="{{ json_encode($productTabsOne) }}"></product-tabs-one>
            @endif
        </div>
    </section>

    <section class="deals-cats">
        <div class="deals-log">
            <img class="img-fluid" src="{{ asset('themes/storefront/public/images/stencil.png') }}">
        </div>
        <div class="deals-log2">
            <img src="{{ asset('themes/storefront/public/images/stencil.png') }}" alt="" class="img-fluid">
        </div>
        <div class="container mb-3 deals-cat">
            <div class="row d-row">
                <div class="col-md-7 col-sm-7 col-12 d-col">
                    <div class="banner-adv banner-countdown jewelry-coundown2 zoom-image gray-image">
                        <a class="adv-thumb-link" href="javascript:void(0)">
                            <img src="{{ asset('themes/storefront/public/images/deal.jpg') }}" alt="">
                        </a>
                        <div class="banner-info text-center">
                            <h3>Deals of the day</h3>
                            <h2>upto 60% off</h2>
                            <a href="javascript:void(0)">shop all</a>

                        </div>
                    </div>
                </div>

                <div class="col-sm-5 col-12 d-col">
                    @if (setting('storefront_product_tabs_2_section_enabled'))
                        <product-tabs-two :data="{{ json_encode($tabProductsTwo) }}"></product-tabs-two>
                    @endif
                </div>
            </div>
        </div>
    </section>

      @php
                
                $token = 'IGQVJXN1FtSEJTcG1pMmFHTW52WFNwMklCSWswYy1CM2pKbE1LV1BTUlZAodnFPWlhNWTBoMDRLdmNEOUtoalQtcUFULUJabWZAsMjMtM29wQllzUXViYWVyUVd4NkNxN19uUTRHNnJNa1UyU1FxRVZAtcQZDZD';
            $url = 'https://graph.instagram.com/me/media?fields=id,username,timestamp,caption,media_url,media_type,permalink&access_token='.$token.'';

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url); // Campaign list 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $res = json_decode( $result, true );

            @endphp
            @if(isset($res['data']))
                <section class="insta-images">
                    <div class="container-fluid">
                        <div class="row">
                                <div class="col-12 col-md-12 text-center">
                                    <div class="sec-tit">
                                        <h3>Shop Our Instagram</h3>
                                    </div>
                                </div>
                        </div>
                        {{-- @if (setting('storefront_top_brands_section_enabled') && $topBrands->isNotEmpty())
                            <top-brands :top-brands="{{ json_encode($topBrands) }}"></top-brands>
                        @endif --}}

                      
                        
                            @if(count($res['data'])>0)
                            <div class="swiper-container mt-1 instaSlider">
                                <div class="swiper-wrapper">
                                    @foreach($res['data'] as $insta)
                                    @if($insta['media_type'] == 'IMAGE')
                                  <div class="swiper-slide">
                                    <a href="{{ $insta['permalink'] }}" data-fancybox="insta">
                                        <img src="{{ $insta['media_url'] }}" class="img-fluid" alt="insta">
                                    </a>
                                  </div>
                                  @endif
                                  @endforeach

                                  
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                            @endif
                    </div>
                </section>
            @endif

    @if (setting('storefront_features_section_enabled'))
        <home-features :features="{{ json_encode($features) }}"></home-features>
    @endif

    @if (setting('storefront_featured_categories_section_enabled'))
        <featured-categories :data="{{ json_encode($featuredCategories) }}"></featured-categories>
    @endif

    @if (setting('storefront_three_column_full_width_banners_enabled'))
        <banner-three-column-full-width :data="{{ json_encode($threeColumnFullWidthBanners) }}"></banner-three-column-full-width>
    @endif

    {{-- @if (setting('storefront_product_tabs_1_section_enabled'))
        <product-tabs-one :data="{{ json_encode($productTabsOne) }}"></product-tabs-one>
    @endif --}}

    {{-- @if (setting('storefront_top_brands_section_enabled') && $topBrands->isNotEmpty())
        <top-brands :top-brands="{{ json_encode($topBrands) }}"></top-brands>
    @endif --}}

    {{-- @if (setting('storefront_flash_sale_and_vertical_products_section_enabled'))
        <flash-sale-and-vertical-products :data="{{ json_encode($flashSaleAndVerticalProducts) }}"></flash-sale-and-vertical-products>
    @endif --}}

    @if (setting('storefront_two_column_banners_enabled'))
        <banner-two-column :data="{{ json_encode($twoColumnBanners) }}"></banner-two-column>
    @endif

    @if (setting('storefront_product_grid_section_enabled'))
        <product-grid :data="{{ json_encode($productGrid) }}"></product-grid>
    @endif

    @if (setting('storefront_three_column_banners_enabled'))
        <banner-three-column :data="{{ json_encode($threeColumnBanners) }}"></banner-three-column>
    @endif

    {{-- @if (setting('storefront_product_tabs_2_section_enabled'))
        <product-tabs-two :data="{{ json_encode($tabProductsTwo) }}"></product-tabs-two>
    @endif --}}

    @if (setting('storefront_one_column_banner_enabled'))
        <banner-one-column :banner="{{ json_encode($oneColumnBanner) }}"></banner-one-column>
    @endif
@endsection
