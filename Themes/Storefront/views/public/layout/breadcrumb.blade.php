@hasSection('breadcrumb')
<section class="inner-banner">
        <div class="breadcrumb">
            <ul class="list-inline">
                <li>
                    <a href="{{ route('home') }}">{{ trans('storefront::layout.home') }}</a>
                </li>

                @yield('breadcrumb')
            </ul>
        </div>
</section>
@endif
