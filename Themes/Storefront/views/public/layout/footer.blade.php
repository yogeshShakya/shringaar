<footer class="footer-wrap">
    <div class="container">
        <div class="list-service-footer">
            <ul class="list-none">
                <li>
                    <a href="#">
                        <img src="{{ asset('themes/storefront/public/images/icon1.png') }}" alt="">
                    </a>
                    <span class="opaci">
                        FREE DELIVERY FROM ₹ 7000
                    </span>
                </li>
                <li>
                    <a href="#">
                        <img src="{{ asset('themes/storefront/public/images/icon2.png') }}" alt="">
                    </a>
                    <span class="opaci">
                        SECURE PAYMENT
                    </span>
                </li>
                <li>
                    <a href="#">
                        <img src="{{ asset('themes/storefront/public/images/icon3.png') }}" alt="">
                    </a>
                    <span class="opaci">
                        100% GUARANTEED
                    </span>
                </li>
                {{-- <li>
                    <a href="#">
                        <img src="{{ asset('themes/storefront/public/images/icon4.png') }}" alt="">
                    </a>
                    <span class="opaci">
                        MONEY BACK GUARANTEED
                    </span>
                </li> --}}
            </ul>
        </div>
    </div>

    <div class="footer2">
        <div class="container">
            <div class="main-footer2">
                <div class="row justify-content-sm-center justify-content-md-start">
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="block-footer2">
                            <h2>Contact Us</h2>
                            <p>Don't hesitate in contacting us for any questions you might have.</p>
                            <ul class="list-none contact-foter2">
                                @if (setting('storefront_address'))
                                    <li>
                                        <i class="las la-map-marker"></i>
                                        <span>{{ setting('storefront_address') }}</span>
                                    </li>
                                @endif
                                 @if (setting('store_phone') && ! setting('store_phone_hide'))
                                    <li>
                                        <i class="las la-phone"></i>
                                        <span>{{ setting('store_phone') }}</span>
                                    </li>
                                @endif

                                @if (setting('store_email') && ! setting('store_email_hide'))
                                    <li>
                                        <i class="las la-envelope"></i>
                                        <span>{{ setting('store_email') }}</span>
                                    </li>
                                @endif

                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-5 col-sm-6 col-12">
                        <div class="block-footer2">
                            <div class="logo-footer2 text-center">
                                <a href="/">
                                    <img src="{{$footer_logo?$footer_logo:asset('themes/storefront/public/images/final-shringaar-white.png') }}" alt="" class="img-fluid">
                                </a>
                            </div>
                            <h2 class="font-18">Follow Us On</h2>
                            @if (social_links()->isNotEmpty())
                                <ul class="list-inline social-links social-network-footer text-center">
                                    @foreach (social_links() as $icon => $socialLink)
                                        <li>
                                            <a href="{{ $socialLink }}">
                                                <i class="{{ $icon }}"></i>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>

                    @if ($footerMenuOne->isNotEmpty())
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="block-footer2">
                            <h2>Quick Links</h2>
                             <ul class="list-none contact-foter2">
                                    @foreach ($footerMenuOne as $menuItem)
                                        <li>
                                            <a href="{{ $menuItem->url() }}" target="{{ $menuItem->target }}">
                                                {{ $menuItem->name }}
                                            </a>
                                        </li>
                                    @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </div>  

            <div class="footer-bottom2">
                <div class="desc copyright-footer text-center">
                    <span>{!! $copyrightText !!} Designed by:</span>
                    <a href="https://ebslon.com/" target="blank">Ebslon Infotech</a>
                </div>
            </div>
        {{-- <div class="footer">
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-5 col-md-8">
                        <div class="contact-us">
                            <h4 class="title">{{ trans('storefront::layout.contact_us') }}</h4>

                            <ul class="list-inline contact-info">
                                @if (setting('store_phone') && ! setting('store_phone_hide'))
                                    <li>
                                        <i class="las la-phone"></i>
                                        <span>{{ setting('store_phone') }}</span>
                                    </li>
                                @endif

                                @if (setting('store_email') && ! setting('store_email_hide'))
                                    <li>
                                        <i class="las la-envelope"></i>
                                        <span>{{ setting('store_email') }}</span>
                                    </li>
                                @endif

                                @if (setting('storefront_address'))
                                    <li>
                                        <i class="las la-map"></i>
                                        <span>{{ setting('storefront_address') }}</span>
                                    </li>
                                @endif
                            </ul>

                            @if (social_links()->isNotEmpty())
                                <ul class="list-inline social-links">
                                    @foreach (social_links() as $icon => $socialLink)
                                        <li>
                                            <a href="{{ $socialLink }}">
                                                <i class="{{ $icon }}"></i>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-5">
                        <div class="footer-links">
                            <h4 class="title">{{ trans('storefront::layout.my_account') }}</h4>

                            <ul class="list-inline">
                                <li>
                                    <a href="{{ route('account.dashboard.index') }}">
                                        {{ trans('storefront::account.pages.dashboard') }}
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ route('account.orders.index') }}">
                                        {{ trans('storefront::account.pages.my_orders') }}
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ route('account.reviews.index') }}">
                                        {{ trans('storefront::account.pages.my_reviews') }}
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ route('account.profile.edit') }}">
                                        {{ trans('storefront::account.pages.my_profile') }}
                                    </a>
                                </li>

                                @auth
                                    <li>
                                        <a href="{{ route('logout') }}">
                                            {{ trans('storefront::account.pages.logout') }}
                                        </a>
                                    </li>
                                @endauth
                            </ul>
                        </div>
                    </div>

                    @if ($footerMenuOne->isNotEmpty())
                        <div class="col-lg-3 col-md-5">
                            <div class="footer-links">
                                <h4 class="title">{{ setting('storefront_footer_menu_one_title') }}</h4>

                                <ul class="list-inline">
                                    @foreach ($footerMenuOne as $menuItem)
                                        <li>
                                            <a href="{{ $menuItem->url() }}" target="{{ $menuItem->target }}">
                                                {{ $menuItem->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if ($footerMenuTwo->isNotEmpty())
                        <div class="col-lg-3 col-md-5">
                            <div class="footer-links">
                                <h4 class="title">{{ setting('storefront_footer_menu_two_title') }}</h4>

                                <ul class="list-inline">
                                    @foreach ($footerMenuTwo as $menuItem)
                                        <li>
                                            <a href="{{ $menuItem->url() }}" target="{{ $menuItem->target }}">
                                                {{ $menuItem->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if ($footerTags->isNotEmpty())
                        <div class="col-lg-4 col-md-7">
                            <div class="footer-links footer-tags">
                                <h4 class="title">{{ trans('storefront::layout.tags') }}</h4>

                                <ul class="list-inline">
                                    @foreach ($footerTags as $footerTag)
                                        <li>
                                            <a href="{{ $footerTag->url() }}">
                                                {{ $footerTag->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="footer-bottom">
                <div class="row align-items-center">
                    <div class="col-md-9 col-sm-18">
                        <div class="footer-text">
                            {!! $copyrightText !!}
                        </div>
                    </div>

                    @if ($acceptedPaymentMethodsImage->exists)
                        <div class="col-md-9 col-sm-18">
                            <div class="footer-payment">
                                <img src="{{ $acceptedPaymentMethodsImage->path }}" alt="accepted payment methods">
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div> --}}
    </div>
    </div>
    
</footer>
