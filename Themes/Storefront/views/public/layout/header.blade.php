<header class="header-wrap">
    <div class="header-wrap-inner">
        <div class="head-log">
            <img src="{{ asset('themes/storefront/public/images/stencil.png') }}">
        </div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-sm col">
                    <div class="logo">
                        <a href="{{ route('home') }}" class="header-logo">
                        @if (is_null($logo))
                            <h3>{{ setting('store_name') }}</h3>
                        @else
                            <img src="{{ $logo }}" alt="logo" class="img-fluid bb-1">
                            
                            <img src="{{ $menu_scroll_logo?$menu_scroll_logo:asset('themes/storefront/public/images/final-shringaar.png') }}" class="img-fluid b-1" alt="logo">
                            {{-- <img src="{{ asset('themes/storefront/public/images/final-shringaar.png') }}" class="img-fluid b-1" alt="logo"> --}}
                        @endif
                    </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-auto col-auto ml-auto header-nav2">
                    <div class="d-flex justify-content-end">
                        <a href="javascript:void(0)" class="d-xl-none hamburger">
                            <span class="h-top"></span>
                            <span class="h-middle"></span>
                            <span class="h-bottom"></span>
                        </a>
                    @include('public.layout.navigation.primary_menu')
                    <div class="header-column-right d-flex">
                    <a href="{{ route('account.wishlist.index') }}" class="header-wishlist">
                        <div class="icon-wrap">
                            <i class="lar la-heart"></i>
                            <div class="count" v-text="wishlistCount"></div>
                        </div>

                        {{-- <span>{{ trans('storefront::layout.favorites') }}</span> --}}
                    </a>

                    <div class="header-cart">
                        <div class="icon-wrap">
                            <i class="las la-shopping-bag"></i>
                            <div class="count" v-text="cart.quantity"></div>
                        </div>

                       {{--  <span v-html="cart.subTotal.inCurrentCurrency.formatted"></span> --}}
                    </div>
                </div>
                </div>
                </div>
            </div>
            {{-- <div class="row flex-nowrap justify-content-between position-relative">
                <div class="header-column-left">
                    <div class="sidebar-menu-icon-wrap">
                        <div class="sidebar-menu-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>

                    <a href="{{ route('home') }}" class="header-logo">
                        @if (is_null($logo))
                            <h3>{{ setting('store_name') }}</h3>
                        @else
                            <img src="{{ $logo }}" alt="logo">
                        @endif
                    </a>
                </div>

                 <header-search
                    :categories="{{ $categories }}"
                    :most-searched-keywords="{{ $mostSearchedKeywords }}"
                    initial-query="{{ request('query') }}"
                    initial-category="{{ request('category') }}"
                >
                </header-search> 

                <div class="header-column-right d-flex">
                    <a href="{{ route('account.wishlist.index') }}" class="header-wishlist">
                        <div class="icon-wrap">
                            <i class="lar la-heart"></i>
                            <div class="count" v-text="wishlistCount"></div>
                        </div>

                        <span>{{ trans('storefront::layout.favorites') }}</span>
                    </a>

                    <div class="header-cart">
                        <div class="icon-wrap">
                            <i class="las la-cart-arrow-down"></i>
                            <div class="count" v-text="cart.quantity"></div>
                        </div>

                        <span v-html="cart.subTotal.inCurrentCurrency.formatted"></span>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</header>
