@if ($subMenus->isNotEmpty())
    <ul class="list-inline sub-menu">
        @foreach ($subMenus as $subMenu)
            <li class="{{ $subMenu->hasItems() ? 'dropdown' : '' }} lsl">
                <a href="{{ $subMenu->hasItems() ? 'javascript:void(0)' : $subMenu->url() }}" target="{{ $subMenu->target() }}" class="ii">
                    {{ $subMenu->name() }}
                </a>
                @if ($subMenu->hasItems())
                    @include('public.layout.navigation.dropdown', ['subMenus' => $subMenu->items()])
                @endif
            </li>
        @endforeach
    </ul>
@endif




