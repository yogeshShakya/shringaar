<nav class="main-nav">
    <ul class="navbar-nav mega-menu horizontal-megamenu flex-xl-row flex-xl-nowrap align-items-xl-center justify-content-xl-end">
        @foreach ($primaryMenu->menus() as $menu)
            @include('public.layout.navigation.menu', ['type' => 'primary_menu'])
        @endforeach
    </ul>
</nav>
